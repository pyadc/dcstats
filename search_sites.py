import haystack
haystack.autodiscover()

import datetime
from haystack.indexes import *
from haystack import site
from statsviewer.models import File


class FileIndex(SearchIndex):
    text = CharField(document=True, use_template=True)
    firstseen = DateTimeField(model_attr='firstseen')
    lastseen = DateTimeField(model_attr='lastseen')

    def get_queryset(self):
        """Used when the entire index for model is updated."""
        return File.objects.all()


site.register(File, FileIndex)