from pyadc import bot
from pyadc import config
from pyadc import adclib
import datetime
import sys,os
import filelist
from multiprocessing import Process, Queue
sys.path.append(os.path.join(os.path.abspath('..'), 'web', 'django_project'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from statsviewer.models import *

moddir = "pyadc"
config.keyDir = os.path.join(moddir, config.keyDir)
config.shareDir = os.path.join(moddir, config.shareDir)

class StatsBotClient(bot.BotClient):
    def handleUserInfo(self, sid, params):
        bot.BotClient.handleUserInfo(self, sid, params)
        if "ID" in params and "NI" in params:
            print sid, params["NI"]
            self.getFile(sid, "files.xml", "lists/"+params["ID"]+"-"+params["NI"]+".xml")         
            
            try:
                user = User.objects.get(clientID=params["ID"])
            except User.DoesNotExist:
                user = User(clientID=params["ID"])
            user.username = params["NI"]
            user.save()
            try:
                uiiold = user.userinfointerval_set.get(end=None)
                uiiold.end = datetime.datetime.now()
                uiiold.save()
            except UserInfoInterval.DoesNotExist:
                pass
            uii = UserInfoInterval(user=user,sid=sid)
            for key in "ID", "I4", "I6", "VE", "EM", "NI", "DE", "TO", "SU", "RF":
                if key in params:
                    setattr(uii, "adc"+key, params[key])
            for key in "U4", "U6", "SS", "SF", "US", "DS", "SL", "AS", "AM", "CT", "AW":
                if key in params:
                    try:
                        setattr(uii, "adc"+key, int(params[key]))
                    except ValueError: pass
            uii.save()

    def handleUserQuit(self, sid):
        try:
            uiiold = UserInfoInterval.objects.get(sid=sid, end=None)
            uiiold.end = datetime.datetime.now()
            uiiold.save()
        except UserInfoInterval.DoesNotExist:
            pass
    
    def handleSearch(self, usersid, params):
        try:
            user = UserInfoInterval.objects.get(end=None, sid=usersid).user
            if "AN" in params:
                if params["AN"].__class__.__name__ == "str": queries = [params["AN"]]
                else: queries = params["AN"]
                searchtext = " ".join(queries)
                if len(searchtext) > 500: searchtext = searchtext[:500]
                sq = SearchQuery(user=user, text=searchtext)
                sq.save()
            elif "TR" in params:
                tth = params["TR"]
                if len(tth) > 40: tth = tth[:40]
                try:
                    file = File.objects.get(tth=tth)
                except File.DoesNotExist:
                    file = File(tth=tth, real=False, size=0)
                    file.save()
                tq = TigerQuery(user=user, file=file)
                tq.save()
        except UserInfoInterval.DoesNotExist: pass
        adclib.HCClient.handleSearch(self, usersid, params)
        
    def handleMessage(self, fromsid, text, private=False):
        if len(text) > 500: text = text[:500]
        user = UserInfoInterval.objects.get(sid=fromsid, end=None).user
        message = Message(user=user, private=private, text=text)
        try:
            message.save()
        except:
            pass
    
    def handlePrivateMessage(self, fromsid, text):
        self.handleMessage(fromsid, text, True)

class StatsBotFactory(bot.BotFactory):
    protocol = StatsBotClient
    dir = moddir

if __name__ == '__main__':
    for uiiold in UserInfoInterval.objects.filter(end=None):
        uiiold.end = datetime.datetime.now()
        uiiold.save()
    from twisted.internet import reactor
    f = StatsBotFactory()
    reactor.connectTCP("localhost", config.dtellaPort, f)
    reactor.run()
