from django.http import HttpResponse
from django.db.models import Avg, Max, Min, Count, Sum
from statsviewer.models import *
from django.template import Context, loader
import datetime, os
import sys
import cairoplot
from django.contrib.auth.decorators import login_required
if sys.argv[0] == "debug.py":
    CACHEDIR = "/srv/http/camdc/debug"
else:
    CACHEDIR = "/srv/http/camdc"

def cache(fn, request, *args):
    extra = "_".join(args)
    if not "nocache" in request.GET and not "debug.py" in sys.argv:
        return HttpResponse(open(os.path.join(CACHEDIR,fn.__name__+"_"+extra+".html"), "r").read())
    else:
        out = fn(request, *args)
        open(os.path.join(CACHEDIR,fn.__name__+"_"+extra+".html"), "w").write(out.encode("utf-8"))
        return HttpResponse(out)


def _index(request):
    total = FileName.objects.aggregate(Count("file__tth"), Sum("file__size"))
    unique = File.objects.aggregate(Count("tth"), Sum("size"))
    bysize = File.objects.order_by("-size")[:10]
    byusers = File.objects.annotate(cnt=Count("users")).order_by("-cnt")[:10]
    bynames = File.objects.annotate(cnt=Count("filename")).order_by("-cnt")[:10]
    byqueries = File.objects.annotate(cnt=Count("tigerquery")).order_by("-cnt")[:10]
    
    byfiles = User.objects.annotate(cnt=Count("file")).order_by("-cnt")[:10]
    byshare = User.objects.annotate(cnt=Sum("file__size")).order_by("-cnt")[:10]
    
    t = loader.get_template("index.html")
    c = Context({
        "files": total["file__tth__count"],
        "files_unique": unique["tth__count"],
        "size": total["file__size__sum"],
        "size_unique": unique["size__sum"],
        "file_tables": [ ("By Size", bysize, "bysize"), ("By Number of Users", byusers, "byusers"),
            ("By Number of Names", bynames, "bynames"), ("By Number of Queries", byqueries, "byqueries") ],
        "user_tables": [ ("By Number of Files", byfiles, False), ("By Share Size", byshare, True) ]
    })
    return t.render(c)

def index(request):
    return cache(_index, request)

def _files(request, q):
    if q == "bysize": tab = ("By Size", File.objects.order_by("-size")[:1000])
    elif q == "byusers": tab = ("By Number of Users", File.objects.annotate(cnt=Count("users")).order_by("-cnt")[:1000])
    elif q == "bynames": tab = ("By Number of Names", File.objects.annotate(cnt=Count("filename")).order_by("-cnt")[:1000])
    elif q == "byqueries": tab = ("By Number of Queries", File.objects.annotate(cnt=Count("tigerquery")).order_by("-cnt")[:1000])
    t = loader.get_template("list.html")
    c = Context({
        "type": "files",
        "file_tables": [ (tab[0], tab[1], q) ],
    })
    return t.render(c)

def files(request, q):
    return cache(_files, request, q)

def _users(request):
    byfiles = User.objects.annotate(cnt=Count("file")).order_by("-cnt")
    byshare = User.objects.annotate(cnt=Sum("file__size")).order_by("-cnt")
    t = loader.get_template("list.html")
    c = Context({
        "type": "users",
        "user_tables": [ ("By Number of Files", byfiles, False), ("By Share Size", byshare, True) ]
    })
    return t.render(c)
    
def users(request):
    return cache(_users, request)

@login_required
def search(request):
    q = request.GET['q']
    f = File.objects.filter(filename__name__icontains=q).distinct()[:20]
    t = loader.get_template("list.html")
    c = Context({
        "type": "files",
        "file_tables": [ (("Search Results", f)) ],
        "nomore": True
    })
    return HttpResponse(t.render(c))

@login_required
def searchspy(request):
    searches = SearchQuery.objects.all().order_by("-when")[:100]
    t = loader.get_template("spy.html")
    c = Context({
        "searches": searches,
        "tiger": False
    })
    return HttpResponse(t.render(c))
    
@login_required
def tigerspy(request):
    searches = TigerQuery.objects.all().order_by("-when")[:100]
    t = loader.get_template("spy.html")
    c = Context({
        "searches": searches,
        "tiger": True
        
    })
    return HttpResponse(t.render(c)) 

def usercount(request):
    now = datetime.datetime.now()
    #now = now - datetime.timedelta(minutes=now.minute, seconds=now.second, microseconds=now.microsecond)
    #dt = datetime.datetime(2011,3,8)
    dt = datetime.datetime(2011,4,24)
    points = []
    labels = []
    values = []
    while dt < now:
        users = UserInfoInterval.objects.filter(start__lt=dt).exclude(end__lt=dt).count()
        points.append((dt,users))
        if dt.hour == 0:
            labels.append(dt.strftime("%d/%m"))
        else:
            labels.append("")
        values.append(int(users))
        dt += datetime.timedelta(hours=1)
    if request.GET.get("format") == "pickle":
        import pickle
        out = pickle.dumps(points)
    else:
        """g = LineGraph(dimensions=(500, 300))
        g.title = "USERS ON CAMDC"
        g.x_title = "Time"
        g.y_title = "Users"
        users = g.Series("Users")
        for point in points:
            users.append(str(point[0]), point[1])
        g.importSeries(users)
        g.render('users.png')
        out = '<img src="/static/users.png" />'"""
        """
        G = GChartWrapper.Line([100,150,20,40])
        G.axes.type('xy')
        G.axes.label("This", "test")
        G.axes.label(50,100)
        G.size(250,100)
        out = G.img()"""
        d = cairoplot.dot_line_plot("users", values, 800, 400, x_labels=labels) #y_bounds=(0,)
        out = '<img src="/static/users.svg" />'
    return HttpResponse(out)
    
def sharecount(request):
    now = datetime.datetime.now()
    #dt = datetime.datetime(2011,3,8)
    dt = datetime.datetime(2011,4,24)
    points = []
    labels = []
    values = {"a":[], "b":[]}
    while dt < now:
        users = UserInfoInterval.objects.filter(start__lt=dt).exclude(end__lt=dt).count()
        values["a"].append(users)
        total = UserInfoInterval.objects.filter(start__lt=dt).exclude(end__lt=dt).aggregate(Sum("user__file__size"))
        #total = FileName.objects.aggregate(Count("file__tth"), Sum("file__size"))
        #unique = File.objects.aggregate(Sum("size")) #Count("tth"),
        if total["user__file__size__sum"]:
            values["b"].append(float(total["user__file__size__sum"])/1024/1024/1024/1024)
        else:
            values["b"].append(0)
        if dt.hour == 0:
            labels.append(dt.strftime("%d/%m"))
        else:
            labels.append("")
        dt += datetime.timedelta(hours=1)
    d = cairoplot.dot_line_plot("graph", values, 800, 400, x_labels=labels) #y_bounds=(0,)
    out = '<img src="/static/graph.svg" />'
    return HttpResponse(out)  

@login_required
def show(request, q, pk):
    t = loader.get_template("single.html")    
    if q == "file": M = File
    elif q == "user": M = User
    elif q == "filename": M = FileName
    o = M.objects.get(pk=int(pk))
    fields = []
    for field in o._meta.fields:
        fields.append((field.name, field.value_to_string(o)))
    c = Context({
        "q": q,
        "fields": fields
    })
    if q == "file":
        c.update({"filenames": o.filename_set.all()})
        c.update({"queries": o.tigerquery_set.all()})
    elif q == "user":
        total = FileName.objects.filter(user=pk).aggregate(Count("file__tth"), Sum("file__size"))
        unique = File.objects.filter(filename__user=pk).aggregate(Count("tth"), Sum("size"))
        bysize = File.objects.filter(filename__user=pk).order_by("-size")[:20]
        bynames = File.objects.filter(filename__user=pk).annotate(cnt=Count("filename")).order_by("-cnt")[:20]
        c.update({
            "files": total["file__tth__count"],
            "files_unique": unique["tth__count"],
            "size": total["file__size__sum"],
            "size_unique": unique["size__sum"],
            "tables": [ ("By Size", bysize), ("By Number of Names", bynames) ]
        })
        
    return HttpResponse(t.render(c))
