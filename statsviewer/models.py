from django.db import models

class User(models.Model):
    username = models.CharField(max_length=50)
    firstseen = models.DateTimeField(auto_now_add=True)
    lastseen = models.DateTimeField(auto_now=True)
    clientID = models.CharField(max_length=40, unique=True)
    
class UserInfoInterval(models.Model):
    start = models.DateTimeField(auto_now_add=True)
    end = models.DateTimeField(null=True)
    user = models.ForeignKey(User)
    sid = models.CharField(max_length=4, null=True)
    adcID = models.CharField(max_length=50, null=True)
    adcI4 = models.CharField(max_length=16, null=True)
    adcI6 = models.CharField(max_length=50, null=True)
    adcU4 = models.IntegerField(null=True)
    adcU6 =  models.IntegerField(null=True)
    adcSS = models.BigIntegerField(null=True)
    adcSF = models.BigIntegerField(null=True)
    adcVE = models.CharField(max_length=50, null=True)
    adcUS =  models.IntegerField(null=True)
    adcDS =  models.IntegerField(null=True)
    adcSL =  models.IntegerField(null=True)
    adcAS =  models.IntegerField(null=True)
    adcAM =  models.IntegerField(null=True)
    adcEM = models.CharField(max_length=50, null=True)
    adcNI = models.CharField(max_length=50, null=True)
    adcDE = models.CharField(max_length=500, null=True)
    adcTO = models.CharField(max_length=50, null=True)
    adcCT =  models.IntegerField(null=True)
    adcAW =  models.IntegerField(null=True)
    adcSU = models.CharField(max_length=50, null=True)
    adcRF = models.CharField(max_length=50, null=True)

class File(models.Model):
    firstseen = models.DateTimeField(auto_now_add=True)
    lastseen = models.DateTimeField(auto_now=True)
    users = models.ManyToManyField(User)
    tth = models.CharField(max_length=40, unique=True)
    size = models.BigIntegerField()
    real = models.BooleanField(default=True)

class FileName(models.Model):
    firstseen = models.DateTimeField(auto_now_add=True)
    lastseen = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=500)
    path = models.CharField(max_length=1000)
    user = models.ForeignKey(User)
    file = models.ForeignKey(File)

class TigerQuery(models.Model):
    when = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    file = models.ForeignKey(File)

class SearchQuery(models.Model):
    when = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    text = models.CharField(max_length=500)

class Message(models.Model):
    when = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    private = models.BooleanField(default=False)
    text = models.CharField(max_length=500)
    
