from django import template
from statsviewer.models import *

register = template.Library()
def bestname(file):
    try: return file.filename_set.latest("id").name
    except FileName.DoesNotExist: return file.tth

def nice_size(n):
    if n:
        for l in "B", "kB", "MB", "GB", "TB":
            if n < 1024 or l == "TB":
                return "%.2f%s" % (n, l)
                break
            else:
                n/=1024.0
    else:
        return unicode(n)

register.filter('bestname', bestname)
register.filter('nice_size', nice_size)
