import os,sys
if len(sys.argv) > 1 and sys.argv[1] == "debug":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'debug_settings'
    sys.argv[0] = "debug.py"
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
sys.path.append(os.path.abspath('.'))
sys.path.append(os.path.abspath('..'))
from django.test.client import Client
c = Client()
for f in [ "/users", "/files/bysize", "/files/byusers", "/files/bynames", "/files/byqueries", "/usercount", "/sharecount", "/" ]:
    response = c.get(f+"?nocache")
    print response.content
