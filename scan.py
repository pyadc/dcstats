import sys,os,re
import time
if len(sys.argv) > 1 and sys.argv[1] == "debug":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'debug_settings'
    sys.argv[0] = "debug.py"
    debug = True
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
    debug = False
from statsviewer.models import User
from statsviewer.models import File
from statsviewer.models import FileName
from Queue import Empty
import gc

from xml.etree.ElementTree import iterparse, tostring, ParseError
from multiprocessing import Process, Queue, Lock

def todb(file, clientID, username):
    global j
    f = open(file, "r")
    try:
        user = User.objects.get(clientID=clientID)
    except User.DoesNotExist:
        return
    i=0
    path = ["/"]
    try:
        for (event, el) in iterparse(f, events=['start', 'end']):
            if event == "start" and el.tag == "File":
                tth = el.get("TTH")
                name = el.get("Name")
                if len(name) > 500: name = name[:500]
                if len(path[-1]) > 1000: path[-1] = path[:1000]
                size = int(el.get("Size"))
                try:
                    file = File.objects.get(tth=tth)
                    if size > 0:
                        file.size = size
                    file.save()
                    try:
                        fn = FileName.objects.get(file=file, name=name, user=user)
                        fn.path = path[-1]
                        fn.save()
                    except FileName.DoesNotExist:
                        fn = FileName(file=file, user=user, name=name, path=path[-1])
                        fn.save()
                except File.DoesNotExist:
                    file = File(tth=tth, size=size)
                    file.save()
                    fn = FileName(file=file, user=user, name=name, path=path[-1])
                    fn.save()
                file.users.add(user)
                i+=1
                j+=1 
                if j % 100 == 0:
                    print "%d\t%d" % (j,i)
                if i > 10:
                    return
            elif el.tag == "Directory":
                if event == "start":
                    path.append(os.path.join(path[-1], el.get("Name")))
                elif event == "end":
                    path.pop()
    except ParseError:
        return

basename = re.compile("^([^\-]*)-(.*)\.xml$")
j = 0

if len(sys.argv) < 2 or debug:
    filelists = Queue()
    
    for file in os.listdir("lists"):
        m = basename.match(file)
        if m:
            filelists.put(file)
    
    def f():
        while True:
            try: fl = filelists.get(block=False)
            except Empty: break
            m = basename.match(fl)
            if time.time() - os.path.getmtime(os.path.join("lists", fl)) < 24*60*60:
                print fl
                todb(os.path.join("lists", fl), m.group(1), m.group(2))
    
    if debug:
        Process(target=f).start()
    else:
        for i in range(0,10):
            Process(target=f).start()
        
else:
    fl = sys.argv[1]
    m = basename.match(fl)
    if time.time() - os.path.getmtime(os.path.join("lists", fl)) < 24*60*60:
        print fl
        todb(os.path.join("lists", fl), m.group(1), m.group(2))
