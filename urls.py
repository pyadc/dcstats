from django.conf.urls.defaults import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


from statsviewer.models import *

urlpatterns = patterns('',
    # Example:
    # (r'^dcstats/', include('dcstats.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    #(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    (r'^accounts/login/$', 'django.contrib.auth.views.login'),
    
    (r'^/?$', 'statsviewer.views.index'),
    (r'^users/?$', 'statsviewer.views.users'),
    (r'^files/(.*)$', 'statsviewer.views.files'),
    (r'^(file|user|filename)/(.*)$', 'statsviewer.views.show'),
    (r'^searchspy/?$', 'statsviewer.views.searchspy'),
    (r'^tigerspy/?$', 'statsviewer.views.tigerspy'),
    (r'^usercount/?$', 'statsviewer.views.usercount'),
    (r'^sharecount/?$', 'statsviewer.views.sharecount'),
    (r'^search/', include('haystack_urls')),

)

"""
try:
    from django_restapi.model_resource import Collection
    from django_restapi.responder import XMLResponder

    xml_uii = Collection(
        queryset = UserInfoInterval.objects.all(),
        permitted_methods = ('GET',),
        responder = XMLResponder(paginate_by=100)
    )
    
    urlpatterns.append((r'^xml/userinfointerval/(.*?)/?$', xml_uii))
except ImportError:
    pass
"""

"""
    
    "django.views.generic.list_detail",
    (r'^(?P<tth>[A-Z]+)/$', "object_detail", {
        "template_name": 'statsviewer/detail.html'
        })
(r'^users/?$',
        ListView.as_view(
            queryset=Poll.objects.order_by('-pub_date')[:5],
            context_object_name='latest_poll_list',
            template_name='polls/index.html')),
    ,"""
