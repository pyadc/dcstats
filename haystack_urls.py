from django.conf.urls.defaults import *
from haystack.views import SearchView
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('haystack.views',
    url(r'^$', login_required(SearchView()), name='haystack_search'),
)
