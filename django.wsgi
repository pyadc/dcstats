#!/usr/bin/python 

import os, sys

for path in ["/home/bjwebb/code/", "/home/bjwebb/code/dcstats/"]:
    if path not in sys.path:
        sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'dcstats.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

